# Angular Form Messages example

[![Build Status](https://travis-ci.org/xebia/angular-form-messages-example.svg?branch=master)](https://travis-ci.org/xebia/angular-form-messages-example)
[![Test Coverage](https://codeclimate.com/github/xebia/angular-form-messages-example/badges/coverage.svg)](https://codeclimate.com/github/xebia/angular-form-messages-example)
[![Code Climate](https://codeclimate.com/github/xebia/angular-form-messages-example/badges/gpa.svg)](https://codeclimate.com/github/xebia/angular-form-messages-example)

This is an example project that demonstrates [Angular Form Messages](http://www.github.com/xebia/angular-form-messages).

## Installation

Run `npm install`.

## Build & development

Run `grunt serve` for preview.

## Testing

Running `grunt` will run al the code quality tests and builds the project.
